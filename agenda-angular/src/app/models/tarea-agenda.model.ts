export class TareaAgenda {
	private	prioridad: boolean;

	constructor(public titulo: string, public fecha: string, public detalles: string, public votes:number = 0) {
		this.prioridad = false;
	}

	esPrioridad(): boolean {
		return this.prioridad;
	}

	setPrioridad(x: boolean) {
		this.prioridad = x;
	}

	voteUp() {
		this.votes++;
	}
	voteDown() {
		this.votes--;
	}
}