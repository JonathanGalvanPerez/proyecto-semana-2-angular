import { TareaAgenda } from './tarea-agenda.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoPrioridadAction, NuevaTareaAction, EliminarTareaAction } from '../models/tareas-agenda-state.model';
import { Injectable } from '@angular/core';

//import { Subject, BehaviorSubject } from 'rxjs'

@Injectable()
export class TareasApiClient {
	tareas: TareaAgenda[];
	//current: Subject<TareaAgenda> = new BehaviorSubject<TareaAgenda>(null);
	constructor(private store: Store<AppState>) {
		this.store.select(state => state.tareas.items).subscribe(items => this.tareas = items);
	}
	add(tarea: TareaAgenda) {
		this.store.dispatch(new NuevaTareaAction(tarea));
	}
	delete(tarea: TareaAgenda) {
		this.store.dispatch(new EliminarTareaAction(tarea));
	}
	getAll(): TareaAgenda[] {
		return this.tareas;
	}
	// priorizar es equivalente a la funcion "elegir" que hace el profe
	priorizar(tarea: TareaAgenda) {
		this.store.dispatch(new ElegidoPrioridadAction(tarea));
		//this.current.next(tarea);
	}
	subscribeOnChange(fn) {
		//this.current.subscribe(fn);
		this.store.select(state => state.tareas.prioridad).subscribe(fn);
	}
	/*getById(id: String): TareaAgenda{
		return this.tareas.filter(tarea => (tarea.id.toString() == id))
	}*/
}