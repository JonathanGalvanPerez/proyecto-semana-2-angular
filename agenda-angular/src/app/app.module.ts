import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



import { AppComponent } from './app.component';
import { ListaTareasComponent } from './lista-tareas/lista-tareas.component';
import { TareaAgendaComponent } from './tarea-agenda/tarea-agenda.component';
import { PlanTareaComponent } from './plan-tarea/plan-tarea.component';
import { FormTareaAgendaComponent } from './form-tarea-agenda/form-tarea-agenda.component';
import { TareasApiClient } from './models/tareas-api-client.model';
import { TareasAgendaState, reducerTareasAgenda, initializeTareasAgendaState, TareasAgendaEffects } from './models/tareas-agenda-state.model';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaTareasComponent},
  { path: 'plan', component: PlanTareaComponent}
];

// redux init
export interface AppState {
  tareas: TareasAgendaState;
}

const reducers: ActionReducerMap<AppState> = {
  tareas: reducerTareasAgenda
};

let reducersInitialState = {
  tareas: initializeTareasAgendaState()
}

// redux fin init

@NgModule({
  declarations: [
    AppComponent,
    ListaTareasComponent,
    TareaAgendaComponent,
    PlanTareaComponent,
    FormTareaAgendaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([TareasAgendaEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    TareasApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
