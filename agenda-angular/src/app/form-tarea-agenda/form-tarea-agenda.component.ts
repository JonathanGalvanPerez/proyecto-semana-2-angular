import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TareaAgenda } from '../models/tarea-agenda.model';
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { fromEvent, combineLatest, Observable } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-tarea-agenda',
  templateUrl: './form-tarea-agenda.component.html',
  styleUrls: ['./form-tarea-agenda.component.css']
})
export class FormTareaAgendaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TareaAgenda>;
  fg: FormGroup;
  searchResults: string[];
  minLongitud = 5;

  constructor(fb: FormBuilder) {
  	this.onItemAdded = new EventEmitter();
  	this.fg = fb.group({
  		titulo: ['', Validators.compose([
  			Validators.required,
  			this.validatorParametrizable(this.minLongitud)
  		])],
  		fecha: ['', Validators.required],
  		detalles: ['']
  	})

  	this.fg.valueChanges.subscribe((form: any) => {
  		console.log("cambio el formulario: ", form);
  	})
  }

  ngOnInit(): void {
    let elemTitulo = <HTMLInputElement>document.getElementById('titulo');
    // Observable de los eventos de teclado del campo titulo
    const entrada = fromEvent(elemTitulo, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 3),
        debounceTime(200),
        distinctUntilChanged(),
      );
    // Observable de archivo json por ajax
    var predicciones = ajax.getJSON('/assets/datos.json')
      .pipe(map(respuesta => respuesta as string[]));
    // Combino ambos para poder hacer la busqueda
    combineLatest(entrada, predicciones).subscribe(([query, opciones]) => {
        this.searchResults = opciones.filter(x => x.includes(query));
      });
  }

  guardar(titulo: string, fecha: string, detalles: string): boolean {
  	const tarea = new TareaAgenda(titulo, fecha, detalles);
  	this.onItemAdded.emit(tarea);
  	return false;
  }

  tituloValidator(control: FormControl): { [s: string]: boolean } {
  	let l = control.value.toString().trim().lenght;
  	if (l > 0 && l < 5) {
  		return { invalidTitulo: true }
  	}
  	return null;
  }

  validatorParametrizable(minLong: number): ValidatorFn {
  	return (control: FormControl): { [s: string]: boolean } | null => {
  		const l = control.value.toString().trim().length;
  		if (l > 0 && l < minLong) {
  			return { minLongTitulo: true };
  		}
  		return null;
  	}
  }

}
