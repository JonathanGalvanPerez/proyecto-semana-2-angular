import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TareaAgenda } from './../models/tarea-agenda.model';
import { TareasApiClient } from './../models/tareas-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';


@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TareaAgenda>;
  updates: string[];

  constructor(public tareasApiClient: TareasApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.tareasApiClient.subscribeOnChange((tarea: TareaAgenda) => {
      if (tarea != null) {
        this.updates.push('Se ha elegido a ' + tarea.titulo)
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(tarea: TareaAgenda) {
	  this.tareasApiClient.add(tarea);
    this.onItemAdded.emit(tarea);
  }

  priorizar(tarea: TareaAgenda){
    //Selecciona la tarea y la mueve al inicio
    this.tareasApiClient.priorizar(tarea);
  }

  eliminar(tarea: TareaAgenda){
    this.tareasApiClient.delete(tarea);
  }

}
