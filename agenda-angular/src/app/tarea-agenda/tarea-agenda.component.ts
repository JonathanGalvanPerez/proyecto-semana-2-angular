import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { TareaAgenda } from './../models/tarea-agenda.model';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../models/tareas-agenda-state.model';


@Component({
  selector: 'app-tarea-agenda',
  templateUrl: './tarea-agenda.component.html',
  styleUrls: ['./tarea-agenda.component.css']
})
export class TareaAgendaComponent implements OnInit {
  @Input() tarea: TareaAgenda;
  @Input() indice: number;
  @HostBinding('attr.class') cssClass = 'card';
  @Output() darPrioridad: EventEmitter<TareaAgenda>;
  @Output() eliminar: EventEmitter<TareaAgenda>;

  constructor(private store: Store<AppState>) {
  	this.darPrioridad = new EventEmitter();
    this.eliminar = new EventEmitter();
  }

  ngOnInit(): void {
  }

  seleccionar():boolean {
  	this.darPrioridad.emit(this.tarea);
  	return false;
  }

  borrar():boolean {
    this.eliminar.emit(this.tarea);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.tarea));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.tarea));
    return false;
  }
}
